<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Test;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/hello/{name}", name="hello")
     * @Template
     */
    public function helloAction($name)
    {

        $this->get('pdf.generator')->generatePDF('<h1>Salut bai '.$name."!!!</h1>");

        return ['name'=>$name];
    }
}
