<?php
/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 23-02-2019
 * Time: 1:51 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Cart;
use AppBundle\Entity\Delivery;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class OrderItemController extends Controller
{
    /**
     * @return Order
     */
    private function getOrder()
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');
        $cart = $em->getRepository(Cart::class)->find($session->get('cart_id'));
        $order = new Order();
        $order->setCart($cart);
        $order->setStatus('pending');
        $order->setUser($this->getUser());
        $em->persist($order);
        $em->flush();

        return $order;
    }

    private function addOrderItems(Order $order){
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');

        /* @var Cart $cart
         * @var OrderItem $orderItem
         */

        $cart = $em->getRepository(Cart::class)->find($session->get('cart_id'));
        $coll = [];
        foreach ($cart->getCartItems() as $cartItem){
            $orderItem = new OrderItem();
            $orderItem->setQuantity($cartItem->getQuantity());
            $orderItem->setPrice($cartItem->getPrice());
            $orderItem->setPart($cartItem->getPart());
            $orderItem->setOrder($order);
            $em->persist($orderItem);
            $em->flush();
            $coll[]=$orderItem;
        }

        $order->setOrderItems($coll);

    }

    /**
     * @return Delivery
     */
    public function addDelivery(Request $request, $order){
        $em=$this->getDoctrine()->getManager();
        $delivery = new Delivery();
        $delivery->setName($request->get('name'));
        $delivery->setSurname($request->get('surname'));
        $delivery->setAddress($request->get('address'));
        $delivery->setTelephone($request->get('telephone'));
        $delivery->setOrder($order);
        $em->persist($delivery);
        $em->flush();

        return $delivery;
    }


    /**
     * @Route("/showOrder", name="showOrder")
     * @Template
     */
    public function showAction(Request $request){
        $order = $this->getOrder();
        $this->addOrderItems($order);
        $delivery = $this->addDelivery($request,$order);

        return[
            'order'=>$order,
            'userLogIn'=>ucfirst($this->container->get('security.token_storage')->getToken()->getUser()->getUserName()),
            'delivery'=>$delivery,
            ];

    }

}