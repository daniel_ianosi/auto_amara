<?php
/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 2/17/2019
 * Time: 11:14 PM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Cart;
use AppBundle\Entity\CartItem;
use AppBundle\Entity\Manufacturer;
use AppBundle\Entity\Model;
use AppBundle\Entity\Part;
use AppBundle\Repository\PartsRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class PartController extends Controller
{
    /**
     * @Route("/parts", name="parts")
     * @Template
     */
    public function showAction()
    {
        $session=$this->get('session');
        $em=$this->getDoctrine()->getManager();
        $model=$em->getRepository(Model::class)->find($session->get('model_id'));
        $manufacturer=$em->getRepository(Manufacturer::class)
            ->find($session->get('manufacturer_id'));
        $parts=$model->getParts();

        return [
            'parts'=>$parts,
            'model'=>$model,
            'manufacturer'=>$manufacturer
        ];
    }

    /**
     * @Route("/part/{id}", name="part")
     * @Template
     */
    public function showPartAction(Request $request, $id)
    {
        $em=$this->getDoctrine()->getManager();
        $part=$em->getRepository(Part::class)->find($id);

        $routing = $this->get('router');
        $formCart = $this->createFormBuilder()
            ->setMethod(Request::METHOD_GET)
            ->setAction($routing->generate('cart-add-item'))
            ->add('id',HiddenType::class,['data'=>$part->getId()])
            ->add('quantity',NumberType::class)
            ->add('add', SubmitType::class, ['label'=>'Adauga'])
            ->getForm();

            return [
                'part'=>$part,
                'formCart' => $formCart->createView(),
            ];
    }

    /**
     *  @Route("/searchParts", name="searchParts" , methods={"GET","HEAD"})
     * @Template
     */
    public function showSearchAction(Request $request)
    {
        $value=$request->get('searchEnter');

        /** @var EntityManager  $em */
        $em=$this->getDoctrine()->getManager();
        $qb=$em->createQueryBuilder();

        $query=$qb->select('p')->from(Part::class,'p')
            ->orWhere('p.name LIKE :name')
            ->orWhere('p.code LIKE :code')
            ->setParameter('name','%'.$value.'%')
            ->setParameter('code','%'.$value.'%')
            ->getQuery();

        $parts=$query->execute();

        return ['parts'=>$parts];

    }
}