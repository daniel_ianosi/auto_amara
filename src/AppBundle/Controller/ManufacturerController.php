<?php
/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 2/17/2019
 * Time: 3:06 PM
 */

namespace AppBundle\Controller;
use AppBundle\Entity\Manufacturer;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\EqualToValidator;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlankValidator;

class ManufacturerController extends Controller
{
    /**
     * @Route("/manufacturers", name="manufacturers")
     * @Template
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->setMethod('GET')
            ->add('manufacturer', EntityType::class,
                [
                // looks for choices from this entity
                'class' => 'AppBundle:Manufacturer',
                'query_builder' => function (EntityRepository $er)
                {
                    return $er->createQueryBuilder('m')
                       ->orWhere('m.type = :type')
                       ->setParameter('type','car');
                }
            ])
            ->add('save', SubmitType::class, ['label' => 'Alege'])
            ->getForm();
        if ($request->get('form', false)){
            $this->get('session')->set('manufacturer_id', $request->get('form')['manufacturer']);
            return $this->redirectToRoute('models');
        }


        if ($this->container->get('security.token_storage')->getToken()->getUser()!='anon.'){
            $userLogIn = $this->container->get('security.token_storage')->getToken()->getUser()->getUserName();
        }else{
            $userLogIn='isNotSet';
        }
        return
            [
                'form' => $form->createView(),
                'userLogIn'=>ucfirst($userLogIn),
            ];
    }

    /**
     * @Route("/manufacturer-select/{id}", name="manufacturer")
     */
    public function selectAction($id) {

    }
}