<?php
/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 2/17/2019
 * Time: 4:23 PM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Manufacturer;
use AppBundle\Entity\Model;
use AppBundle\Entity\Test;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ModelController extends Controller
{

    /**
     * @Route("/models", name="models")
     * @Template
     */
    public function showAction()
    {
    $session=new Session();
    $em=$this->getDoctrine()->getManager();
    $session = $this->get('session');
    $currentManufacturer=$em->getRepository(Manufacturer::class)
        ->find($session->get('manufacturer_id'));

    $models=$currentManufacturer->getModels();
    return ['models'=>$models, 'manufacturer'=>$currentManufacturer];
    }

    /**
     * @Route("/model-select/{id}", name="model")
     */
    public function selectAction($id) {
        $em = $this->getDoctrine()->getManager();
        $this->get('session')->set('model_id', $id);
        return $this->redirectToRoute('parts');
    }

}