<?php
/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 26-02-2019
 * Time: 2:24 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\InvoceItem;
use AppBundle\Entity\Invoice;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class InvoiceItemController extends Controller
{

    /**
     * @return Invoice
     */
    private function getInvoice(Order $order)
    {

        $em = $this->getDoctrine()->getManager();
        $invoice = new Invoice();
        $invoice->setOrder($order);
        $em->persist($invoice);
        $em->flush();

        return $invoice;
    }

    /**
     * @return Invoice
     */
    private function addInvoiceItems($orderId)
    {
        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository(Order::class)->find($orderId);
        $invoice = $this->getInvoice($order);
        $orderItems = $order->getOrderItems();
        /**
         * @var OrderItem $orderItem
         */
        foreach ($orderItems as $orderItem) {

            $invoiceItem = new InvoceItem();
            $invoiceItem->setPrice($orderItem->getPrice());
            $invoiceItem->setQuantity($orderItem->getQuantity());
            $invoiceItem->setInvoice($invoice);
            $em->persist($invoiceItem);
            $em->flush();
        }
        return $invoice;
    }

    /**
     * @Route("/showInvoice/{orderId}/{action}", name="showInvoice")
     */
    public function showAction($orderId, $action)
    {
        $invoice = $this->addInvoiceItems($orderId);
        $html = $this->render('AppBundle:invoice_item:show.html.twig', ['invoice' => $invoice]);

        if ($action=='I') {
            $this->get('pdf.generator')->generatePDF($html, $action,$invoice->getId());
        }else{
            $this->get('pdf.generator')->generatePDF($html, $action,$invoice->getId());
        }
    }

}