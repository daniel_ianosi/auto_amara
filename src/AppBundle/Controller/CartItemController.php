<?php
/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 21-02-2019
 * Time: 7:46 PM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Cart;
use AppBundle\Entity\Manufacturer;
use AppBundle\Entity\Model;
use AppBundle\Entity\Part;
use AppBundle\Entity\CartItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\SecurityBundle\Tests\Functional\Bundle\AclBundle\Entity\Car;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CartItemController extends Controller
{
    /**
     * @return Cart
     */
    private function getCart()
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');
        if ($session->has('cart_id')) {
            $cart = $em->getRepository(Cart::class)->find($session->get('cart_id'));
        } else {
            $cart = new Cart();
            $em->persist($cart);
            $em->flush();
            $session->set('cart_id', $cart->getId());
        }

        return $cart;
    }

    private function getCartItem(Part $part)
    {
        foreach ($this->getCart()->getCartItems() as $cartItem) {
            if ($part->getId() == $cartItem->getPart()->getId()){
                return $cartItem;
            }
        }

        return new CartItem();
    }

    /**
     * @Route("/cartItems", name="cartItems")
     * @Template
     */
    public function showAction()
    {
        if ($this->container->get('security.token_storage')->getToken()->getUser()!='anon.'){
            $userLogIn = $this->container->get('security.token_storage')->getToken()->getUser()->getUserName();
        }else{
            $userLogIn='isNotSet';
        }

            return
            [
                'cart' => $this->getCart(),
                'userLogIn'=>ucfirst($userLogIn),

            ];
    }

    /**
     * @Route("/add-item", name="cart-add-item")
     */
    public function addItemAction(Request $request){
        $quantity = $request->get('form')['quantity'];
        if ($quantity>=1){
            $em = $this->getDoctrine()->getManager();
            $part = $em->getRepository(Part::class)->find($request->get('form')['id']);
            $cartItem = $this->getCartItem($part);
            $cartItem->setCart($this->getCart());
            $cartItem->setPart($part);
            $cartItem->setQuantity($cartItem->getQuantity()+$request->get('form')['quantity']);
            $cartItem->setPrice($part->getPrice());
            $em->persist($cartItem);
            $em->flush();

            return $this->redirectToRoute('cartItems');
        }else{
            return $this->redirect('part/'.$request->get('form')['id']);
        }
    }

    /**
     * @Route("/cartItems/{partId}", name="updateQuantity")
     */
    public function updateQuantityAction(Request $request, $partId)
    {
        $em = $this->getDoctrine()->getManager();
        $part = $em->getRepository(Part::class)->find($partId);
        $cartItem = $this->getCartItem($part);
        $cartItem->setQuantity($request->get('quantity'));
        $em->persist($cartItem);
        $em->flush();

        return $this->redirectToRoute('cartItems');
    }

    /**
     * @Route("/deleteItem/{partId}", name="deleteItem")
     */
    public function deleteItemAction($partId)
    {
        $em = $this->getDoctrine()->getManager();
        $part = $em->getRepository(Part::class)->find($partId);
        $cartItem = $this->getCartItem($part);
        $em->remove($cartItem);
        $em->flush();

        return $this->redirectToRoute('cartItems');
    }

}