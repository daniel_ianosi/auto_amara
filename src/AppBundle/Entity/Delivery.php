<?php
/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 25-02-2019
 * Time: 4:44 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Delivery
*
 * @ORM\Table(name="deliveries")
* @ORM\Entity
*/
class Delivery
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255, nullable=false)
     */
    private $surname;
    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=500, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="integer", length=10, nullable=false)
     */
    private $telephone;

    /**
     * @var \AppBundle\Entity\Cart
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Order")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * })
     */
    private $order;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Delivery
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Delivery
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return Delivery
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

      /**
     * @return int
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param int $telephone
     * @return Delivery
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Delivery
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return Cart
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Cart $order
     * @return Delivery
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }





}