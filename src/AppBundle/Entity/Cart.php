<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carts
 *
 * @ORM\Table(name="carts", indexes={@ORM\Index(name="id", columns={"id"}), @ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="user_id_2", columns={"user_id"})})
 * @ORM\Entity
 */
class Cart
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var \AppBundle\Entity\CartItem[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CartItem", mappedBy="cart")
     */
    private $cartItems = [];

    /**
     * @var \AppBundle\Entity\Order
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Order", mappedBy="cart")
     */
    private $order;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Cart
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return CartItem[]
     */
    public function getCartItems()
    {
        return $this->cartItems;
    }

    /**
     * @param CartItem[] $cartItems
     * @return Cart
     */
    public function setCartItems($cartItems)
    {
        $this->cartItems = $cartItems;

        return $this;
    }

    public function getTotalPrice()
    {
        $total = 0;
        foreach ($this->getCartItems() as $cartItem){
            $total+=$cartItem->getPrice()*$cartItem->getQuantity();
        }

        return $total;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return Cart
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }


}
