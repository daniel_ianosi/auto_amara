<?php
namespace AppBundle\Services;
/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 2/18/2019
 * Time: 9:31 PM
 */
class PdfGenerator
{
    public $tcpdf;


    public function generatePDF($html,$action,$invoice)
    {
        $pdfObj = $this->getTcpdf()->create();

        $pdfObj->AddPage();
        $pdfObj->writeHTML($html, true, false, true, false, '');

        $pdfObj->Output('Factura nr.'.$invoice.'.pdf', $action);
    }

    /**
     * @return mixed
     */
    public function getTcpdf()
    {
        return $this->tcpdf;
    }

    /**
     * @param mixed $tcpdf
     * @return PdfGenerator
     */
    public function setTcpdf($tcpdf)
    {
        $this->tcpdf = $tcpdf;
        return $this;
    }


}