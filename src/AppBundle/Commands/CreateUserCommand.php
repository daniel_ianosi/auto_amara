<?php
/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 2/20/2019
 * Time: 7:29 PM
 */

namespace AppBundle\Commands;

use AppBundle\Entity\Manufacturer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Argument\ArgumentInterface;

class CreateUserCommand extends ContainerAwareCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'amara:create-user';

    protected function configure()
    {
        $this->addArgument('type', InputArgument::REQUIRED, 'Define the type');
        $this->addOption('models','m');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //echo "Hello world!!";

        //$output->writeln("Hello world!!");

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $man = $em->getRepository(Manufacturer::class)
            ->findBy(['type'=>$input->getArgument('type')]);

        foreach ($man as $manufacturer){
            /** @var Manufacturer  $manufacturer*/
            $output->writeln((string)$manufacturer);
            if ($input->getOption('models')){
                foreach ($manufacturer->getModels() as $model){
                    $output->writeln("\t\t".(string)$model);
                }
            }
        }

    }
}