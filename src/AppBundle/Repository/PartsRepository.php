<?php
namespace AppBundle\Repository;
use AppBundle\Entity\Part;
use Doctrine\ORM\EntityRepository;

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 2/18/2019
 * Time: 1:53 PM
 */
class PartsRepository extends EntityRepository
{

    public $result;

    public function getSearchedParts($value)
    {
        $qb=$this->getEntityManager()->createQueryBuilder();

        $query=$qb->select('p')->from(Part::class,'p')
            ->orWhere('p.name LIKE :name')
            ->orWhere('p.code LIKE :code')
            ->setParameter('name','%'.$value.'%')
            ->setParameter('code','%'.$value.'%')
            ->getQuery();
        $parts=$query->execute();

    }





}